mkchromecast (0.3.9~git20200902+db2964a-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Replace youtube-dl with yt-dlp (Closes: #1024216).

 -- Bastian Germann <bage@debian.org>  Mon, 18 Sep 2023 10:18:36 +0000

mkchromecast (0.3.9~git20200902+db2964a-2) unstable; urgency=medium

  * Also drop .gitignore files not present in the tarball.
  * Work around a weird issue with mkchromecast.getch not being installed.

 -- Andrej Shadura <andrewsh@debian.org>  Fri, 16 Oct 2020 08:47:59 +0200

mkchromecast (0.3.9~git20200902+db2964a-1) unstable; urgency=medium

  [ Muammar El Khatib ]
  * Changing the way mkchromecast-* packages recommend installation of
    mkchromecast. Closes: #909822. Thanks to Ruben Undheim for providing
    a patch.
  * The Debian packaging also was adapted and improved by Alec Leamas.

  [ Stefan Rücker ]
  * debian: Remove links.
  * debian: Update to dh compat v13.
  * debian/control: Remove libav-tools.

  [ Andrej Shadura ]
  * New co-maintainer.
  * Update to the new upstream snapshot,
    declare hard dependency on the newer pychromecast (Closes: #965249).
  * Set Vcs-* to salsa.debian.org.
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Improve the Python bits of the packaging:
    - Add the Python dependencies to Build-Depends so that pybuild can use
      them.
    - Add missing dependencies.
    - Make sure Python files are installed with pybuild as well.
    - Use pybuild.
  * Install the icon again
  * Add a watch line to track upstream snapshots.
  * Wrap and sort debian/control.
  * Remove ${python3:Depends} from binary packages not shipping any Python
    code.
  * Drop no longer relevant README.source.

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 07 Oct 2020 15:41:27 +0200

mkchromecast (0.3.8.1-1) unstable; urgency=medium

  * New upstream release fixing bug happening when no devices were found.

 -- Muammar El Khatib <muammar@debian.org>  Sun, 24 Dec 2017 09:31:34 -0500

mkchromecast (0.3.8-1) unstable; urgency=medium

  * New upstream release.
  * Now the package works with python3.
  * Bump Standards-Version to 4.1.2.

 -- Muammar El Khatib <muammar@debian.org>  Sat, 23 Dec 2017 16:26:15 -0500

mkchromecast (0.3.7.1-1) unstable; urgency=medium

  *  New upstream release.

 -- Muammar El Khatib <muammar@debian.org>  Sat, 20 May 2017 14:50:34 -0400

mkchromecast (0.3.7+git20170414-1) unstable; urgency=medium

  * New git revision.

 -- Muammar El Khatib <muammar@debian.org>  Sat, 15 Apr 2017 15:41:29 -0400

mkchromecast (0.3.7+git20170410-1) unstable; urgency=medium

  * New git revision.
  * Support video casting with nodejs.
  * Changing documentation from python mkchromecast.py to just mkchromecast.
    Closes: #857846.

 -- Muammar El Khatib <muammar@debian.org>  Mon, 10 Apr 2017 19:49:55 -0400

mkchromecast (0.3.7+git20170130-2) unstable; urgency=medium

  * Updated gir1.2-notify-0.7 dependency. Closes: #855442.
  * Updated debian/copyright. Closes: #854998.

 -- Muammar El Khatib <muammar@debian.org>  Tue, 31 Jan 2017 15:11:44 -0500

mkchromecast (0.3.7+git20170130-1) unstable; urgency=medium

  * New git revision.
  * Dropped alsa-base dependency in favor of kmod. Closes: #852531.

 -- Muammar El Khatib <muammar@debian.org>  Fri, 30 Dec 2016 12:30:48 -0500

mkchromecast (0.3.6-3) unstable; urgency=medium

  * Now the desktop file places mkchromecast's icon in the correct category.

 -- Muammar El Khatib <muammar@debian.org>  Wed, 28 Sep 2016 16:35:54 +0200

mkchromecast (0.3.6-2) unstable; urgency=medium

  * debian/control:
    - mkchromecast-pulseaudio and mkchromecast-alsa were moved to the Suggests
    field.

 -- Muammar El Khatib <muammar@debian.org>  Wed, 21 Sep 2016 02:03:49 +0200

mkchromecast (0.3.6-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Added new mkchromecast-alsa, and mkchromecast-pulseaudio dependency
      packages.
    - Removed pavucontrol, pulseaudio-utils, and pulseaudio from
      mkchromecast's Depends field.
    - Improved descriptions.
  * Fails to "Open pavucontrol and select the mkchromecast sink." bug has been
    fixed (Closes: #835608).
  * mkchromecast can cast without Pulseaudio.

 -- Muammar El Khatib <muammar@debian.org>  Mon, 19 Sep 2016 20:55:48 +0200

mkchromecast (0.3.5-2) unstable; urgency=medium

  * Updated README.Debian file.
  * debian/control:
    - Added pavucontrol, pulseaudio-utils, and pulseaudio to the Depends field.
    - Removed mkchromecast-doc from the Suggests field.

 -- Muammar El Khatib <muammar@debian.org>  Sat, 27 Aug 2016 18:26:35 +0200

mkchromecast (0.3.5-1) unstable; urgency=medium

  * New upstream release.

 -- Muammar El Khatib <muammar@debian.org>  Fri, 26 Aug 2016 12:36:58 +0200

mkchromecast (0.3.4-2) unstable; urgency=medium

  * debian/control:
    - Improved package description.
  * Fixed an error in desktop file.
  * debian/README.source:
    - Improved description.

 -- Muammar El Khatib <muammar@debian.org>  Mon, 22 Aug 2016 13:21:54 +0200

mkchromecast (0.3.4-1) unstable; urgency=medium

  * Initial official debian release (Closes: #834400).

 -- Muammar El Khatib <muammar@debian.org>  Fri, 19 Aug 2016 20:59:36 +0200

mkchromecast (0.3.3-1) unstable; urgency=medium

  * New upstream release.

 -- Muammar El Khatib <muammar@debian.org>  Tue, 16 Aug 2016 10:34:47 +0200

mkchromecast (0.3.2-1) unstable; urgency=medium

  * New upstream release.

 -- Muammar El Khatib <muammar@debian.org>  Mon, 15 Aug 2016 00:53:54 +0200

mkchromecast (0.3.1-1) unstable; urgency=medium

  * New upstream release.

 -- Muammar El Khatib <muammar@debian.org>  Fri, 12 Aug 2016 18:30:17 +0200

mkchromecast (0.3.0-1) unstable; urgency=medium

  * New upstream release.

 -- Muammar El Khatib <muammar@debian.org>  Mon, 11 Jul 2016 23:40:01 +0200

mkchromecast (0.2.9.1-1) unstable; urgency=medium

  * This revision fixes a segfault in the system tray.

 -- Muammar El Khatib <muammar@debian.org>  Wed, 29 Jun 2016 21:44:00 +0200

mkchromecast (0.2.9-1) unstable; urgency=medium

  * New upstream release.

 -- Muammar El Khatib <muammar@debian.org>  Wed, 29 Jun 2016 11:37:01 +0200

mkchromecast (0.2.8-1) unstable; urgency=medium

  * New upstream release.

 -- Muammar El Khatib <muammar@debian.org>  Tue, 21 Jun 2016 23:47:28 +0200

mkchromecast (0.2.7-1) unstable; urgency=medium

  * New upstream release.

 -- Muammar El Khatib <muammar@debian.org>  Thu, 16 Jun 2016 13:48:11 +0200

mkchromecast (0.2.6-2) unstable; urgency=medium

  * Problem with icons fixed.

 -- Muammar El Khatib <muammar@debian.org>  Fri, 10 Jun 2016 14:02:25 +0200

mkchromecast (0.2.6-1) unstable; urgency=medium

  * Initial release.

 -- Muammar El Khatib <muammar@debian.org>  Fri, 06 May 2016 15:47:56 +0200
